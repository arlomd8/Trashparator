﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Detector : MonoBehaviour
{
    // Declare the variables.
    public string nameTag;
    public AudioClip audioBenar, audioSalah;
    private AudioSource mediaPlayerBenar, mediaPlayerSalah;
    public Text textScore;

    // Initialize the variable at the start.
    private void Start()
    {
        // Add and assign the audio of mediaPlayerBenar.
        mediaPlayerBenar = gameObject.AddComponent<AudioSource>();
        mediaPlayerBenar.clip = audioBenar;

        // Add and assign the audio of mediaPlayerSalah.
        mediaPlayerSalah = gameObject.AddComponent<AudioSource>();
        mediaPlayerSalah.clip = audioSalah;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // To check the tag of the collided object.
        if (collision.tag.Equals(nameTag))
        {
            // Add the score.
            Data.score += 10;

            // Update the score in the UI 
            textScore.text = Data.score.ToString();

            // Destroy the object.
            Destroy(collision.gameObject);

            // Play the audio.
            mediaPlayerBenar.Play();
        }
        else
        {
            // Add the score.
            Data.score -= 15;

            // Update the score in the UI 
            textScore.text = Data.score.ToString();

            // Destroy the object.
            Destroy(collision.gameObject);

            // Play the audio.
            mediaPlayerSalah.Play();
        }
    }
}
