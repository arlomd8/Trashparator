﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    private void Update()
    {
        // To check the keyboard input from user.
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            // To quit from the application.
            Application.Quit();
        }  
    }
}
