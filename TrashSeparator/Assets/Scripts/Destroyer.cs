﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    // To check the collision of another object that have Collider2D and isTrigger.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Destroy collided object.
        Destroy(collision.gameObject);
    }
}
