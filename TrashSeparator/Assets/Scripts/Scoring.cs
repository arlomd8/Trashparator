﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scoring : MonoBehaviour
{
    private void Update()
    {
        // To check whether the score is below zero to load the game over scene.
        if (Data.score < 0)
        {
            // To load GameOver Scene.
            SceneManager.LoadScene("GameOver");
        }
    }
}
