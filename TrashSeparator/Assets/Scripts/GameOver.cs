﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    // Initialize the timer variable.
    private float timer = 0;

    private void Update()
    {
        // To increment the timer.
        timer += Time.deltaTime;

        // To check the timer to load GamePlay Scene and reset score.
        if(timer > 2)
        {
            // Reset the score.
            Data.score = 0;

            // Load the Scene named "GamePlay".
            SceneManager.LoadScene("GamePlay");
        }
    }
}
