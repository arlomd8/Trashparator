﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Variables to be the timer
    public float pauseTime = 0.8f;
    float timer;

    // Array of Gameobject.
    public GameObject[] trash;

    private void Update()
    {
        // Increment the timer.
        timer += Time.deltaTime;

        // Check the timer to generate new random trash gameobject.
        if (timer > pauseTime)
        {
            int i = Random.Range(0, trash.Length);
            Instantiate(trash[i], transform.position, transform.rotation);
            timer = 0;
        }  
    }
}
