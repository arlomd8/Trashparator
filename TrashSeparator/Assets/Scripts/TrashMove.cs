﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashMove : MonoBehaviour
{
    float speed = 3f;
    public Sprite[] sprites;
    private Vector3 screenPoint;
    private Vector3 offset;
    private float firstY;

    //Initialize the sprites with randomized value at the start.
    private void Start()
    {
        int i = Random.Range(0, sprites.Length);
        this.GetComponent<SpriteRenderer>().sprite = sprites[i];
    }

    private void Update()
    {
        // To move the trash game object to left.
        float move = (speed * Time.deltaTime * -1f) + transform.position.x;
        transform.position = new Vector3(move, transform.position.y);
    }

    // To get the object position when the user clicked the trash object.
    private void OnMouseDown()
    {
        firstY = transform.position.y;
        screenPoint = Camera.main.ScreenToWorldPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    // To move the object based on the cursor position when the user drag the trash object.
    private void OnMouseDrag()
    {
        Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
        transform.position = curPosition;
    }

    // To reset the position of the trash object when the user release the mouse click
    private void OnMouseUp()
    {
        transform.position = new Vector3(transform.position.x, firstY, transform.position.z);
    }
}
