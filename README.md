Trashparator
===
![](https://gitlab.com/arlomd8/Trashparator/-/raw/main/Trashparator.gif)

# Introduction
Hello my name is [Arlo Mario Dendi](https://gitlab.com/arlomd8). Welcome to this project. This project was aimed to create a mobile casual game. I started this project to fulfill the task of competency certification held by Game Technology Major from Politeknik Elektronika Negeri Surabaya. The game can be played in Android.

# General Info
There are 2 types of trash organic and non organic. The game will let you to separate the trash into its correct type of bin. If its correct you'll get 10 points, but if it's wrong you'll get -15 points. When your score is below zero, the game is over.
<br>

# Technologies
This game created with : 
- Unity 2020.1.15f1 (LTS)
